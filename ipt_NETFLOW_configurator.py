#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ssh_helper

import argparse
import networkx
from typing import List

"""
Remove the ipt_NETFLOW module if it exists and then insert it with the new parameters
"""
MODPROBE_TEMPLATE = "sudo modprobe -r ipt_NETFLOW;" \
                    "sudo modprobe ipt_NETFLOW destination={collector_ip}:{port} protocol=9 active_timeout={active_timeout}"

IPTABLES_DELETE_LINE_TEMPLATE = "sudo ip6tables -D {table} -j NETFLOW"
IPTABLES_COLLECT_LINE_TEMPLATE = "sudo ip6tables -I {table} -j NETFLOW"


def _write_iptables_commands(router_sessions, iptables_commands: List[str]) -> List[str]:
    outputs = ssh_helper.run_commands_on_many_hosts(router_sessions, iptables_commands)
    return outputs


def _write_modprobe_commands(router_sessions, modprobe_commands: List[str]) -> List[str]:
    outputs = ssh_helper.run_commands_on_many_hosts(router_sessions, modprobe_commands)
    return outputs


def _build_iptables_lines(number: int, template: str) -> List[str]:
    """
    Build the iptables to collect all netflow traffic

    :param number: Build this many copies. There is nothing node-unique about the iptables command.
    :param template: Use this template. Should be one of the constants defined above.
    """
    line = " && ".join([template.format(
        table=table,
    ) for table in ["INPUT", "OUTPUT", "FORWARD"]])
    return [line] * number


def _build_modprobe_lines(netgraph: networkx.Graph, port_nums: List[int], collector_node: str) -> List[str]:
    """
    Construct a command line to load ipt_NETFLOW for each border router with the port number it
    should send data to
    """
    collector_ip = netgraph._node[collector_node]['management-ip']
    lines: List[str] = [MODPROBE_TEMPLATE.format(
        collector_ip=collector_ip,
        port=port,
        active_timeout=15, # Report active flows every 15 seconds
    ) for port in port_nums]
    return lines


def _get_port_nums(netgraph: networkx.Graph, border_routers: List[str]) -> List[int]:
    """
    Read the 'sensor_port' property for each border_router in the netgraph
    """
    port_nums: List[int] = [netgraph._node[router]['sensor_port'] for router in border_routers]
    return port_nums


def configure(netgraph: networkx.Graph, collector_node: str, border_routers: List[str]) -> None:
    """
    Configure the controller node to be a SiLK NetFlow v9 collector

    MODIFIES netgraph to have the listening port information needed by ipt_NETFLOW_configurator

    :param netgraph: networkx graph object representing the network
    :param collector_node: Node which is running the SiLK collector as represented in the graph
    :param border_routers: List of nodes to whom we are listening
    :return: Output from the SSH commands
    """
    router_sessions = [netgraph._node[node]['session'] for node in border_routers]

    port_nums: List[int] = _get_port_nums(netgraph, border_routers)
    iptables_delete_lines: List[str] = _build_iptables_lines(len(border_routers), IPTABLES_DELETE_LINE_TEMPLATE)
    modprobe_lines: List[str] = _build_modprobe_lines(netgraph, port_nums, collector_node)
    iptables_collect_lines: List[str] = _build_iptables_lines(len(border_routers), IPTABLES_COLLECT_LINE_TEMPLATE)

    # Deleting the iptables rules is expected to fail when no such rules exist. Run it with no error checking.
    ssh_helper.unchecked_run_commands_on_many_hosts(router_sessions, iptables_delete_lines)
    _write_modprobe_commands(router_sessions, modprobe_lines)
    _write_iptables_commands(router_sessions, iptables_collect_lines)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Configure the ipt_NETFLOW on border routers to send data to the central collector")

    args = parser.parse_args()
    print("This library is not currently executable")
