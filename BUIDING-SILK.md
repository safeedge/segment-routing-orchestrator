# Build libfixbuf2
## Download
wget https://tools.netsa.cert.org/releases/libfixbuf-2.3.0.tar.gz
sudo apt install libglib2.0-dev

## Extract
tar -xvf  libfixbuf-2.3.0.tar.gz
mkdir libfixbuf-build
cd libfixbuf-build

## Build
/tmp/libfixbuf-2.3.0/configure
make -j8
sudo make install
sudo ldconfig

# Build SILK
## Official setup directions
https://tools.netsa.cert.org/confluence/pages/viewpage.action?pageId=23298051
https://tools.netsa.cert.org/silk/silk-install-handbook.html#x1-410004

## Download
wget https://tools.netsa.cert.org/releases/silk-3.18.1.tar.gz

## Extract
tar -xvf  silk-3.18.1.tar.gz
mkdir silk-build
cd silk-build

## Build
sudo apt install libpython3-dev libfixbuf3-dev
../silk-3.18.1/configure --enable-ipv6 --with-python=/usr/bin/python3 --with-libfixbuf
make -j8
sudo make install
sudo ldconfig

## Setup
https://blog.because-security.com/t/opensource-netflow-collection-with-silk-flowbat-and-how-to-perform-data-analysis/81

https://tools.netsa.cert.org/confluence/pages/viewpage.action?pageId=23298051
