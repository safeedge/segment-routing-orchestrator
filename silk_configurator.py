#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ssh_helper

import argparse
from collections import namedtuple
import networkx
from typing import List

SensorLine = namedtuple('SensorLine', ['uuid', 'name', 'description',])

"""
Build a valid silk.conf file by reading from the template and filling in the gaps
The template defines the following placeholders:
sensor_lines: Should be replaced with a series of lines of the format described by SILK_CONF_SENSOR_LINE_TEMPLATE
sensor_names: Should be replaced with a space-separated list of the names defined for each sensor
"""
SILK_CONF_TEMPLATE_FILENAME="./silk.conf.template"

"""
Define the existence of a sensor
uuid <int>: Some ID which is unique among all defined sensors
name <str>: Some name which is unique among all defined sensors
description <str>: Some optional description of the sensor
"""
SILK_CONF_SENSOR_LINE_TEMPLATE="sensor {uuid} {name} \"{description}\""

FILE_PUSH_COMMAND_TEMPLATE= "sudo mkdir -p /{path}/ && sudo chown $USER /{path}/ && cat <<EOF >/{path}/{filename}\n{data}\nEOF"

COLLECTOR_LAUNCH_COMMAND="mkdir -p /{data_path}/{log_dir} && kill $(cat /{data_path}/{log_dir}/rwflowpack.pid) 2>/dev/null;" \
                         "rwflowpack --sensor-configuration=/{data_path}/sensors.conf --root-directory=/{data_path}/ --log-directory=/{data_path}/{log_dir}/"

SENSOR_CONF_PROBE_BLOCK_TEMPLATE="""probe {name} netflow-v9
 listen-on-port {portnum}
 protocol udp
end probe"""

"""
This group block describes the experimental network IP block 
"""
SENSOR_CONF_GROUP_EXPT_BLOCK="""group expt-network
 ipblocks fd00::/16
end group"""

"""
This group block describes all IPv4 addresses
"""
SENSOR_CONF_GROUP_IPV4_BLOCK="""group ipv4
 ipblocks 0.0.0.0/1
 ipblocks 128.0.0.0/1
end group"""

SENSOR_CONF_SENSOR_BLOCK_TEMPLATE="""sensor {name}
 netflow-v9-probes {name}
 internal-ipblocks @expt-network
 discard-when source-ipblocks @ipv4 # Discard ALL IPv4 traffic
 external-ipblocks remainder
end sensor"""

def _update_netgraph(netgraph, border_routers: List[str], listening_ports: List[int]) -> None:
    """
    Update the netgraph entry for each sensor with information about how it should communicate to the collector

    Adds "sensor_port" key to each border_router node with the information about which port to communicate to
    :param netgraph:
    :param sensor_lines:
    :return:
    """
    for idx in range(len(border_routers)):
        router = border_routers[idx]
        port = listening_ports[idx]
        netgraph._node[router]['sensor_port'] = port


def _write_collector_launch(session, collector_launch_command: str) -> str:
    output = ssh_helper.run_commands_on_many_hosts([session], [collector_launch_command])[0]
    return output


def _write_sensors_conf(session, sensor_lines: List[SensorLine], port_nums: List[int]) -> None:
    probes: List[str] = []
    groups: List[str] = []
    sensors: List[str] = []

    for idx in range(len(sensor_lines)):
        line = sensor_lines[idx]
        portnum = port_nums[idx]
        probe = SENSOR_CONF_PROBE_BLOCK_TEMPLATE.format(
            name = line.name,
            portnum = portnum,
        )
        sensor = SENSOR_CONF_SENSOR_BLOCK_TEMPLATE.format(
            name = line.name,
        )
        probes.append(probe)
        sensors.append(sensor)

    groups.extend([SENSOR_CONF_GROUP_EXPT_BLOCK, SENSOR_CONF_GROUP_IPV4_BLOCK])
    
    sensors_conf = "\n\n".join(probes + groups + sensors)

    command = FILE_PUSH_COMMAND_TEMPLATE.format(filename="sensors.conf", path="/data/", data=sensors_conf)

    ssh_helper.run_commands_on_many_hosts([session], [command])


def _write_silk_conf(session, sensor_lines: List[SensorLine]) -> None:
    with open(SILK_CONF_TEMPLATE_FILENAME, 'r') as input:
        silk_conf_template: str = input.read()

    sensor_names: str = " ".join([line.name for line in sensor_lines])
    sensor_line_block: str = "\n".join(
        [
            SILK_CONF_SENSOR_LINE_TEMPLATE.format(
                uuid=line.uuid,
                name=line.name,
                description=line.description,
            )
        for line in sensor_lines])

    silk_conf = silk_conf_template.format(
        sensor_lines=sensor_line_block,
        sensor_names=sensor_names,
    )

    command = FILE_PUSH_COMMAND_TEMPLATE.format(filename="silk.conf", path="/data/", data=silk_conf)

    ssh_helper.run_commands_on_many_hosts([session], [command])


def _build_collector_launch_command() -> str:
    return COLLECTOR_LAUNCH_COMMAND.format(
        data_path="/data/",
        log_dir="/logs/",
    )


def _build_port_nums(sensor_lines: List[SensorLine]) -> List[int]:
    """
    :param sensor_lines: List of sensor information
    :return: List of ports each sensor should connect to
    """
    # Construct ports starting from 18000 from uuid and hope it's not in use
    port_nums = [18000 + line.uuid for line in sensor_lines]
    return port_nums


def _build_sensor_lines(border_routers: List[str]) -> List[SensorLine]:
    """
    Assign uuid, names, and descriptions for all border routers

    Does NOT modify netgraph

    :param border_routers: List of routers to treat as sensors
    :return:
    """
    next_uuid = 0
    sensor_lines: List[SensorLine] = []

    for router in border_routers:
        uuid = next_uuid
        next_uuid += 1
        # There are some rules on sensor names, so we can't safely just use the node name, unfortunately
        name = "S{uuid}".format(uuid=uuid)
        description = router
        sensor_lines.append(SensorLine(uuid=uuid, name=name, description=description))

    return sensor_lines


def configure(netgraph: networkx.Graph, controller_node: str, border_routers: List[str]) -> None:
    """
    Configure the controller node to be a SiLK NetFlow v9 collector

    MODIFIES netgraph to have the listening port information needed by ipt_NETFLOW_configurator

    :param netgraph: networkx graph object representing the network
    :param controller_node: Node which is running the SiLK collector as represented in the graph
    :param border_routers: List of nodes to whom we are listening
    :return: Output from the SSH commands
    """
    collector_session = netgraph._node[controller_node]['session']

    sensor_lines: List[SensorLine] = _build_sensor_lines(border_routers)
    port_nums: List[int] = _build_port_nums(sensor_lines)
    launch_command: str = _build_collector_launch_command()

    _write_silk_conf(collector_session, sensor_lines)
    _write_sensors_conf(collector_session, sensor_lines, port_nums)
    _write_collector_launch(collector_session, launch_command)

    _update_netgraph(netgraph, border_routers, port_nums)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Configure SiLK to listen for netflow data from all edge switches")

    args = parser.parse_args()
    print("This library is not currently executable")
