This repository contains everything needed to run the segment routing demo on Emulab. More details to come as code is actually written.

## Setup
Since the code in this repository is written in Python, there is no need to compile anything, but the environment must first be setup. This only needs to be done once.

1. First, make sure you have the Python 3 virtualenv command installed. In Ubuntu 16.04 and Fedora, the package is called python3-virtualenv.
2. Set up the virtual environment by running the command `virtualenv-3 env` from within the folder containing this repository
3. Populate the virtual environment using the provided list of requirements:
```
source env/bin/activate # Activate the environment
pip install -r requirements.txt # Use pip to install required packages
```
You are now set up!

## Usage
These instructions will be updated as the code is actually written

Before you will be able to run any commands, activate the virtual environment by running `source env/bin/activate`

`orchestrator.py` automatically does everything required, and can be run directly on an Emulab node to parse
the on-node net information or can be fed a NetJSON NetGraph to configure. Please refer to `orchestrator --help`

## Library Documentation

#### 1. topomap_parser.py
This is both a program which can be run on its own, mostly for testing purposes, and a library which is used by the following programs to parse emulab's topology into [NetJSON](netjson.org) NetworkGraph for usage with the NetworkX network graph library.

When used as a library, the most interesting method is `parse_topomap_to_netjson`, which returns a NetJSON-formatted dictionary. When run standalone, the program has built-in help by passing the `--help` flag.

The topology is parsed using the information in `/var/emulab/boot/topomap` to get basic information, and management IP addresses are found using DNS records. In many situations, topomap_parser will only properly work if run on one of the nodes of an experiment.

For description of the non-standard fields used, see the NetworkGraph section of this document

#### 2. add_routable_ipv6_addrs.py
This is an executable library which provides several methods for gathering the necessary information to construct IPv6 Unique Local Addresses, construct them, and push them to the hosts

The suggested usage is:

 - Construct a NetJSON NetworkGraph using topomap_parser
 - Use the [netdiff](https://github.com/ninuxorg/netdiff) library to convert the NetJSON to a networkx graph
 - Use ssh_helper to log in to each host in the network and annotate the graph with logged-in sessions
 - Use the netgraph to call construct_ULAs
 - Use the result of construct_ULAs and the network graph to call add_ULAs_to_hosts
 - Use the result of construct_ULAs to call add_interfaces_to_netgraph to add the interfaces and the associated IPv6 addresses to the NetGraph
    - This adds an `interfaces` property to the nodes in the graph which maps the (Linux) interface name to the ULA IPv6 address
 - Use ssh_helper to log out of each host in the network

 Additionally, this library has a method `add_default_routes` which is useful for assigning default routes to edge devices

#### 3. frr_configurator.py
This is an executable library which supports configuring and managing FRR (Free Range Routing)

All methods in this library expect to be run on a NetGraph which has been annotated with the `interfaces` field by `add_routable_ipv6_addrs`

The most useful methods are:
  - configure_nodes: Configure and enable zebra and ospf6d
  - start_frr_on_network: Start frr on all nodes in the network
  - stop_frr_on_network: Stop frr on all nodes in the network

#### 4. osfp_sniffer_configurator.py
Support installing and controlling the OSPF sniffer

The most useful methods are:
  - clone_repo_on_network:    Download a repo, presumably the one which contains the OSPF sniffer code
  - start_sniffer_on_network: Run the OSPF sniffer in the background
    - Starting two sniffers is likely to cause problems, chief among which being the original PID file being overwritten
  - stop_sniffer_on_network:  Kill the OSPF sniffer running in the background
    - If stop is run multiple times, the PID file will be invalid, so there is the unlikely event of another process being assigned the same PID and killed

#### 5. sysctl_configurator.py
This is an executable library which supports running sysctl commands

The only currently required command enables IPv6 segment routing

Everything can be automatically accomplished by the configure_nodes method


## TODO
Once this codebase is finished, this section should go away. But great works are never finished, merely abandoned.

The basic goal is to take all the scripts from <https://gitlab.flux.utah.edu/safeedge/sripv6-linux> and convert them to not assume anything about the experiment topology

1. Really need some program which requests the bandwidth of each link from every node.
  - This shouldn't actually be too difficult, I just didn't think about it until too late
2. Replace `start_all_ovs.sh`
  - With something which launches netsa agents on the desired nodes
3. Figure out why running multiple OSPF sniffers causes problems
  - Then fix those problems
  - Then go back to launching OSPF on every node


## NetworkGraph
For the most part, standard NetJSON NetworkGraph fields are used, and those are documented [here](http://netjson.org/rfc.html#rfc.section.4)

At the top-level, no optional fields are defined. This may change in the future to add human-relevant information

The 'local-addresses' key gets defined by the `topomap_parser.py`, but it is not used by the rest of the scripts

Each Node defines the following properties:
 - 'lans': A mapping of Emulab LANs a Node is connected to and its IP address on that lan
 - 'management-ip': A single remote-accessible IP or hostname. The interface to which this address belongs on the node is ignored by the automatic scripts
 - If the orchestrator's `--no-ipv6-assign` flag is used, it is important to define an 'interfaces' property
   - If `--no-ipv6-assign` is not used (the default) then the orchestrator will attempt to assing IPv6 addresses to all non-management interfaces and use all interfaces for the 'interfaces' list
   - 'interfaces' is a mapping of interface name to IPv6 address on that interface
   - It is not required to be an exhaustive list of all interfaces on the node, but it should contian all interfaces which you want the orchestrator to configure (for frr's ospfd, etc.)

 The `management-ip` field is required for use by the automated scripts, the `lans` field is not used

Each Link defines the following properties:
 - 'netmask': The netmask of the Emulab LAN which corresponds to this link
