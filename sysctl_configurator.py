#!/usr/bin/env python3

# Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ssh_helper

import argparse
import getpass
from netdiff import NetJsonParser
from typing import List


"""
Build a valid sysctl command by using one SYSCTL_COMMAND_TEMPLATE then as many space-separated
SYSCTL_LINE_TEMPLATEs as desired
"""
SYSCTL_COMMAND_TEMPLATE = "sudo sysctl {lines}"
SYSCTL_SEG6_LINE_TEMPLATE = "net.ipv6.conf.{iface}.seg6_enabled=1"
SYSCTL_FORWARDING_LINE_TEMPLETE = "net.ipv6.conf.{iface}.forwarding=1"


def build_forwarding_sysctl_command(interfaces: List[str]):
    """
    Construct the sysctl command to enable ipv6 forwarding on all of the listed interfaces

    :param interfaces: list of interface names
    :return: sysctl configuration command
    """
    lines = []
    for interface in interfaces:
        lines.append(SYSCTL_FORWARDING_LINE_TEMPLETE.format(iface=interface))

    return SYSCTL_COMMAND_TEMPLATE.format(lines=str.join(" ", lines))


def build_seg6_sysctl_command(interfaces: List[str]):
    """
    Construct the sysctl command to enable segment routing on all of the listed interfaces

    :param interfaces: list of interface names
    :return: sysctl configuration command
    """
    lines = []
    for interface in interfaces:
        lines.append(SYSCTL_SEG6_LINE_TEMPLATE.format(iface=interface))

    # Hack - Do this better when in less of a hurry
    # Also enable ipv6 forwarding
    for interface in interfaces:
        lines.append(SYSCTL_FORWARDING_LINE_TEMPLETE.format(iface=interface))

    return SYSCTL_COMMAND_TEMPLATE.format(lines=str.join(" ", lines))


def build_seg6_sysctl_commands_for_network(netgraph, ignore_nodes: List[str]=None):
    """
    Build the command string to enable SRv6 for all nodes in the network

    :param netgraph: networkx graph
    :param ignore_nodes: List of nodes to not configure
    :return: mapping of hosts to configurations
    """
    if ignore_nodes is None: ignore_nodes = []

    commands = []

    for host in netgraph.nodes:
        if host in ignore_nodes: continue
        interfaces = list(netgraph._node[host]['interfaces'].keys())
        interfaces.append('all') # Also configure to not disallow seg6 forwarding system-wide
        commands.append(build_seg6_sysctl_command(interfaces))

    return commands


def configure_nodes(netgraph, ignore_nodes: List[str]=None):
    """
    Configure every node in the network to allow segment routing

    :param netgraph: networkx graph object representing the network
    :param ignore_nodes: List of nodes to not configure
    :return: Output from the SSH commands
    """
    if ignore_nodes is None: ignore_nodes = []

    hosts = [host for host in netgraph.nodes if host not in ignore_nodes]
    sessions = list(map(lambda host : netgraph._node[host]['session'], hosts))

    seg6_sysctl_commands = build_seg6_sysctl_commands_for_network(netgraph, ignore_nodes)

    # Push configurations commands to all nodes
    return ssh_helper.run_commands_on_many_hosts(sessions, seg6_sysctl_commands)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Configure Free Range Routing's OSPFv6 and Zebra for all nodes in the network")
    parser.add_argument("--in-file", action='store', type=str, required=True,
                        help="Path to the NetJSON file to parse")
    parser.add_argument("--username", action='store', type=str, default=getpass.getuser(),
                        help="Username to use on all hosts. Defaults to current user's username")
    parser.add_argument("--stop", action='store', type=bool, default=False,
                        help="Stop all services (after writing config files)")

    args = parser.parse_args()

    netgraph = NetJsonParser(file=args.in_file)
    ssh_helper.network_graph_login(netgraph.graph, args.username)

    outputs = configure_nodes(netgraph.graph)

    ssh_helper.network_graph_logout(netgraph.graph)
