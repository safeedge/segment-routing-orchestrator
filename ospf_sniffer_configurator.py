#!/usr/bin/env python3

# Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import getpass
from netdiff import NetJsonParser
import networkx
import re
from typing import List, Optional

import ssh_helper

DEFAULT_CLONE_PATH="/tmp/sniffer/"
DEFAULT_CONTROLLER='core0'
DEFAULT_CONTROLLER_PORT = 8080 # While this could theoretically be any port, the SDN controller does not accept it as a parameter...
DEFAULT_EXECUTABLE='main.py'
DEFAULT_LOGFILE="/tmp/sniffer.log"
DEFAULT_PIDFILE="/tmp/sniffer.pid"
DEFAULT_REPO_BRANCH='release'
DEFAULT_REPO_URL="https://gitlab.flux.utah.edu/safeedge/ospfv3_monitor.git"


def _generate_repo_clone_command(repo: str, branch: str, path: str) -> str:
    return "git clone -b {branch} --single-branch -- {repo} {path};".format(repo=repo, branch=branch, path=path)


def _generate_environment_create_command(path:str) -> str:
    return "pushd {path} && " \
           "virtualenv -p /usr/bin/python2 env; " \
           "source env/bin/activate && " \
           "pip install -r requirements.txt && " \
           "deactivate && " \
           "popd".format(path=path)


def _generate_repo_update_command(path: str) -> str:
    return "pushd {path}; git pull; popd".format(path=path)


def _generate_start_command(path:str=DEFAULT_CLONE_PATH,
                            executable: str=DEFAULT_EXECUTABLE,
                            controller: str=DEFAULT_CONTROLLER,
                            port: int=DEFAULT_CONTROLLER_PORT,
                            logfile: str=DEFAULT_LOGFILE,
                            pidfile: str=DEFAULT_PIDFILE,
                            ) -> str:
    """
    Generate a command which launches the OSPF sniffer

    Does not do any initialization, such as downloading the sniffer

    :param path:       Path into which the sniffer was cloned
    :param executable: Filename of the sniffer daemon from the repository root
    :param controller: Hostname or IP address to which sniffer reports should be sent
    :param port:       Port on the server listening for OSPF updates
    :param logfile:    File to which output from the sniffer should be written
    :param pidfile:    File to which the PID will be written
    :return:
    """
    return "pushd {path}; source env/bin/activate; sudo $(which python) {executable} --controller={controller} --port={port} --log-file={logfile} & echo $! > {pidfile} && disown; deactivate; popd".format(
        path=path,
        executable=executable,
        controller=controller,
        port=port,
        logfile=logfile,
        pidfile=pidfile,
    )

def _generate_stop_command(pidfile: str=DEFAULT_PIDFILE) -> str:
    return "sudo kill $(cat {pidfile}); rm -f {pidfile}".format(pidfile=pidfile)


def clone_repo_on_network(graph: networkx.Graph,
                          repo: str=DEFAULT_REPO_URL,
                          branch: str=DEFAULT_REPO_BRANCH,
                          path: str=DEFAULT_CLONE_PATH,
                          nodes: List[str]=None,
                          ):
    """
    Clone the specified repository and branch on each node in the given list

    If the passed path is already in use, attempts to update the repository in that directory

    :param graph:  networkx.Graph representation of the network
    :param repo:   Repository to clone
    :param branch: Branch in the repository to clone
    :param path:   Directory into which to clone the repository
    :param nodes: Nodes in the network which should have the repository cloned; defaults to all nodes if unspecified
    :return:
    """
    command = _generate_repo_clone_command(repo=repo, branch=branch, path=path)
    hosts = nodes or graph.nodes

    commands = [command for host in hosts]
    sessions = [graph._node[host]['session'] for host in hosts]

    # If we try to clone into an existing directory, git will return an error
    # (Obviously this will get messed up on a non-English system. Sorry.)
    allowed_exception = ".*fatal: destination path \S+ already exists and is not an empty directory"
    sessions_needing_updating = []
    try:
        outputs = ssh_helper.run_commands_on_many_hosts(sessions, commands)
    except ssh_helper.SSHCommandErrorError as e:
        while e is not None:
            output = str.join('', e.output.splitlines()) # Get rid of linebreaks (otherwise we get one every 80 characters)
            if re.match(allowed_exception, output):
                # No problem: Hopefully we tried to re-clone the repository
                sessions_needing_updating.append(e.session)
                e = e.next
            else:
                raise

    # Update anything which needed updating
    commands = [_generate_repo_update_command(path)] * len(sessions_needing_updating)
    outputs = ssh_helper.run_commands_on_many_hosts(sessions_needing_updating, commands)

    # Setup the virtual environment
    commands = [_generate_environment_create_command(path)] * len(sessions)
    outputs = ssh_helper.run_commands_on_many_hosts(sessions, commands)
    pass


def start_sniffer_on_network(graph: networkx.Graph,
                             path:str=DEFAULT_CLONE_PATH,
                             executable: str=DEFAULT_EXECUTABLE,
                             controller: str=DEFAULT_CONTROLLER,
                             port: int=DEFAULT_CONTROLLER_PORT,
                             pidfile: str=DEFAULT_PIDFILE,
                             nodes: List[str]=None):
    """
    Start the sniffer daemon on specified nodes in the network

    :param graph:      networkx.Graph representation of the network
    :param path:       path into which the repository has been cloned
    :param executable: executable to execute
    :param controller: node which is listening for the OSPF reports
    :param port:       port on the controller listening for OSPF updates
    :param pidfile:    file to write the PID of the daemon
    :param nodes:      nodes which should have the daemon run on them; defaults to all nodes in the graph
    :return:
    """
    command = _generate_start_command(path=path, executable=executable, controller=controller, port=port, pidfile=pidfile)
    hosts = nodes or graph.nodes

    commands = [command for host in hosts]
    sessions = [graph._node[host]['session'] for host in hosts]

    # Since this command puts the sniffer in the background, the naive default error checking does not work
    # First the command is echoed
    echos = ssh_helper.unchecked_run_commands_on_many_hosts(sessions, commands)
    # Next we check for potential outputs
    outputs = list(map(lambda s: ssh_helper._get_output(s, timeout=1), sessions))

    # Some real basic error checking
    errors = None
    for index in range(0, len(outputs)):
        # The only good output from a backgrounded script with stdout being redirected to a file is silence
        if not len(outputs[index]) == 0:
            errors = ssh_helper.SSHCommandErrorError(sessions[index],
                                                     outputs[index],
                                                     code=999,
                                                     next=errors)
    if errors is not None: raise errors


def stop_sniffer_on_network(graph: networkx.Graph,
                            pidfile: str=DEFAULT_PIDFILE,
                            nodes: List[str] = None):
    """
    Stop the sniffer on specified nodes in the network
    """
    hosts = nodes or graph.nodes

    sessions = [graph._node[host]['session'] for host in hosts]
    command = _generate_stop_command(pidfile=pidfile)
    commands = [command] * len(hosts)

    try:
        output = ssh_helper.run_commands_on_many_hosts(sessions, commands)
    except ssh_helper.SSHCommandErrorError as e:
        # If the PID file does not exist, cat will have an error trying to read it. This is not a problem, since it probably just
        # means the sniffer is not running or has already been stopped
        acceptable_error_cat = r".*{command}\s+cat: {pidfile}: No such file or directory.*".format(command=re.escape(command), pidfile=re.escape(pidfile))

        # If the sniffer cannot be killed, it probably means that it has already died for some reason. Since our goal was to stop it, mission accomplished
        acceptable_error_kill = r".*kill: \([0-9]+\) - No such process.*".format(command=re.escape(command), pidfile=re.escape(pidfile))
        while e is not None:
            output = str.join(' ', e.output.splitlines()) # Get rid of newlines
            if re.match(acceptable_error_cat, output) or re.match(acceptable_error_kill, output):
                # Presumably this is a sign of the stop command being run more than once or before the sniffer was started: no problem
                e = e.next
                continue
            raise
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Configure and control the OSPF path sniffer for all nodes in the network")
    parser.add_argument("--in-file", action='store', type=str, required=True,
                        help="Path to the NetJSON file to parse")
    parser.add_argument("--username", action='store', type=str, default=getpass.getuser(),
                        help="Username to use on all hosts. Defaults to current user's username")
    parser.add_argument("--stop", action='store_true',
                        help="Stop all sniffers")
    parser.add_argument("--ignore-regex", action='store', type=str, default='.*ovs.*',
                        help="Regex used to determine which nodes, if any, should be ignored (Default: \".*ovs.*\")")
    parser.add_argument("--pid-file", action='store', type=str, default=DEFAULT_PIDFILE,
                        help="File to write sniffer's PID (Default: {default})".format(default=DEFAULT_PIDFILE))
    parser.add_argument("--log-file", action='store', type=str, default=DEFAULT_LOGFILE,
                        help="File to write sniffer's output (Default: {default})".format(default=DEFAULT_LOGFILE))
    parser.add_argument("--controller-name", action='store', type=str, default=DEFAULT_CONTROLLER,
                        help="Hostname or IP of the node which is listening to the OSPF reports (Default: {default})".format(default=DEFAULT_CONTROLLER))
    parser.add_argument("--controller-port", action='store', type=int, default=DEFAULT_CONTROLLER_PORT,
                        help="Port number on the server listening for OSPF reports (Default: {default})".format(default=DEFAULT_CONTROLLER_PORT))
    parser.add_argument("--repo-path", action='store', type=str, default=DEFAULT_REPO_URL,
                        help="(git) Repository which should be downloaded for the OSPF sniffer (Default: {default})".format(default=DEFAULT_REPO_URL))
    parser.add_argument("--repo-branch", action='store', type=str, default=DEFAULT_REPO_BRANCH,
                        help="Name of the branch in the repository to check out (Default: {default})".format(default=DEFAULT_REPO_BRANCH))
    parser.add_argument("--clone-path", action='store', type=str, default=DEFAULT_CLONE_PATH,
                        help="Directory where the repository should be downloaded (Default: {default})".format(default=DEFAULT_CLONE_PATH))

    args = parser.parse_args()

    netgraph = NetJsonParser(file=args.in_file)
    ssh_helper.network_graph_login(netgraph.graph, args.username)

    nodes = netgraph.graph.nodes
    for node in netgraph.graph._node.values():
        if re.match(args.ignore_regex, node['label']):
            nodes.remove(node)

    if args.stop:
        stop_sniffer_on_network(netgraph.graph)
    else:
        clone_repo_on_network(netgraph.graph,
                              repo=args.repo_path,
                              branch=args.repo_branch,
                              path=args.clone_path,
                              nodes=nodes)
        start_sniffer_on_network(netgraph.graph,
                                 path=args.clone_path,
                                 controller=args.controller_name,
                                 port=args.controller_port,
                                 pidfile=args.pid_file,
                                 nodes=nodes)

    ssh_helper.network_graph_logout(netgraph.graph)
