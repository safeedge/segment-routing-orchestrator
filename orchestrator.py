#!/usr/bin/env python3

# Copyright (C) 2018 Simon Redman <sredman@cs.utah.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# For future reference, here is the OSPFv6 RFC: https://tools.ietf.org/html/rfc2740.html

import add_routable_ipv6_addrs
import frr_configurator
import ipt_NETFLOW_configurator
import ospf_sniffer_configurator
import silk_configurator
import ssh_helper
import sysctl_configurator
import topomap_parser

import argparse
import getpass
import json
from netdiff import NetJsonParser
import re
import logging

LOG = logging

DEF_CONTROLLER_NAME = ospf_sniffer_configurator.DEFAULT_CONTROLLER
DEF_BORDER_REGEX = '^ovs.*'
DEF_HOST_REGEX = '^host.*'

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Setup an Emulab experiment for Segment Routing")
    parser.add_argument("-d","--debug",action="store_true")
    parser.add_argument("--topomap-file", action='store', default='/var/emulab/boot/topomap', type=str,
                        help="Path to the emulab topomap file to parse")
    parser.add_argument("--netgraph-file", action='store', type=str, required=False,
                        help="Path to the NetJSON file to parse. Skips parsing the netgrap from the topomap-file")
    parser.add_argument("--netgraph-write", action='store', type=str, required=False,
                        help="(Optional) Path to write the final NetJSON file")
    parser.add_argument("--username", action='store', type=str, default=getpass.getuser(),
                        help="Username to use on all hosts. Defaults to current user's username")
    parser.add_argument("--no-ipv6-assign", action='store_true',
                        help="Do not assign IPv6 addresses - Requires that the 'interfaces' property is defined in the incoming netjson")
    parser.add_argument("--no-frr", action='store_true',
                        help="Do not start the FRR daemons")
    parser.add_argument("--no-sniffer", action='store_true',
                        help="Do not start the OSPF sniffer daemons")
    parser.add_argument("--controller-name", action='store', type=str,
                        help="Hostname or IP of the node which is acting as the controller for all collectors (Default: {default})".format(default=ospf_sniffer_configurator.DEFAULT_CONTROLLER))
    parser.add_argument("--controller-port", action='store', type=int, default=ospf_sniffer_configurator.DEFAULT_CONTROLLER_PORT,
                        help="Port number on the server listening for OSPF reports (Default: {default})".format(default=ospf_sniffer_configurator.DEFAULT_CONTROLLER_PORT))
    parser.add_argument("--border-regex", action='store', type=str,
                        help="Regex to distinguish border switch nodes by label (Default \"^ovs.*\")")
    parser.add_argument("--host-regex", action='store', type=str,
                        help="Regex to distinguish host nodes by label (Default \"^host.*\")")

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if not args.netgraph_file:
        netgraph = NetJsonParser(data=topomap_parser.parse_topomap_to_netjson(args.topomap_file))
    else:
        netgraph = NetJsonParser(file=args.netgraph_file)

    if args.netgraph_write:
        with open(args.netgraph_write, 'w') as outfile:
            json.dump(netgraph.json(dict=True), outfile)

    LOG.debug("logging in to nodes (%s)"
              % (",".join([node['label'] for node in netgraph.graph._node.values()])))
    ssh_helper.network_graph_login(netgraph.graph, args.username)

    # Skip adding ipv6 addresses to the management interfaces
    management_ips = [node["management-ip"] for node in netgraph.graph._node.values()]

    if not args.no_ipv6_assign:
        LOG.debug("assigning ipv6 addresses to nodes (%s)"
                  % (",".join([node['label'] for node in netgraph.graph._node.values()])))

        ULA_map = add_routable_ipv6_addrs.construct_ULAs(netgraph.graph, ignore_addrs=management_ips)

        add_routable_ipv6_addrs.add_ULAs_to_hosts(netgraph.graph, ULA_map)
        add_routable_ipv6_addrs.add_interfaces_to_netgraph(netgraph.graph, ULA_map)

    # Prepare a list of nodes which act as an edge switch and should thus be ignore for router-related activites.  Note that if no regex is supplied, 
    border_nodes = []
    border_regex = args.border_regex or DEF_BORDER_REGEX
    for node in netgraph.graph.nodes:
        node_name = netgraph.graph._node[node]['label']
        if re.match(border_regex, node_name):
            border_nodes.append(node)

    # Find the controller_node.  If we were given a command-line controller_name, we use that; otherwise, we fall back to the netjson node with a "controller" role; otherwise, we try the default regex.
    controller_node = None
    if args.controller_name:
        controller_node = [node for node in netgraph.graph._node if netgraph.graph._node[node]['label'] == args.controller_name].pop()
    else:
        for node in netgraph.graph.nodes:
            if not "roles" in netgraph.graph._node[node]:
                continue
            if "controller" in netgraph.graph._node[node]["roles"]:
                controller_node = node
                break
        if not controller_node:
            controller_node = [node for node in netgraph.graph._node if netgraph.graph._node[node]['label'] == DEF_CONTROLLER_NAME].pop()
    controller_name = netgraph.graph._node[controller_node]['label']

    LOG.debug("controller node is '%s'" % (controller_name,))

    # Prepare a list of nodes which are "customer host nodes" and should thus be ignored for core network-related activities
    host_nodes = []
    host_regex = args.host_regex or DEF_HOST_REGEX
    for node in netgraph.graph.nodes:
        node_name = netgraph.graph._node[node]['label']
        if re.match(host_regex, node_name):
            host_nodes.append(node)

    # Prepare lists of roles ("sniffer", "netflow").
    sniffers = []
    netflow = []
    for node in netgraph.graph.nodes:
        if not "roles" in netgraph.graph._node[node]:
            continue
        if "sniffer" in netgraph.graph._node[node]["roles"]:
            sniffers.append(node)
        if "netflow" in netgraph.graph._node[node]["roles"]:
            netflow.append(node)

    # If we didn't get border_regex args, then try to use the role info from
    # the netjson file, then fall back to border_regex/host_regex defaults.
    ospf_nodes = netgraph.graph.nodes
    if (args.border_regex or args.host_regex) or not sniffers:
        for node in border_nodes + host_nodes:
            ospf_nodes.remove(node)
    else:
        ospf_nodes = sniffers
    if args.border_regex or not netflow:
        netflow_nodes = border_nodes
    else:
        netflow_nodes = netflow

    LOG.debug("configuring FRR on nodes (%s)"
              % (",".join([node['label'] for node in netgraph.graph._node.values()])))
    frr_configurator.configure_nodes(netgraph.graph)
    LOG.debug("configuring sysctls on nodes (%s)"
              % (",".join([node['label'] for node in netgraph.graph._node.values()])))
    sysctl_configurator.configure_nodes(netgraph.graph)

    if not args.no_sniffer:
        LOG.debug("installing ospf sniffer on nodes (%s)"
                  % (",".join(ospf_nodes)))
        ospf_sniffer_configurator.clone_repo_on_network(netgraph.graph, nodes=ospf_nodes)
        ospf_sniffer_configurator.stop_sniffer_on_network(netgraph.graph, nodes=ospf_nodes)  # Stopping with the app not running is not great, but better than starting twice

    if not args.no_frr:
        LOG.debug("restarting FRR on nodes (%s)"
                  % (",".join([node['label'] for node in netgraph.graph._node.values()])))

        frr_configurator.start_frr_on_network(netgraph.graph)
    if not args.no_sniffer:
        LOG.debug("starting up sniffers on nodes (%s)"
                  % (",".join(ospf_nodes)))

        ospf_sniffer_configurator.start_sniffer_on_network(netgraph.graph,
                                                           controller=controller_name,
                                                           port=args.controller_port,
                                                           nodes=ospf_nodes)

    LOG.debug("configuring SiLK on nodes (%s)"
              % (",".join(netflow_nodes)))
    silk_configurator.configure(netgraph.graph,
                                controller_node=controller_node,
                                border_routers=netflow_nodes,
                                )
    LOG.debug("configuring ipt_NETFLOW on nodes (%s)"
              % (",".join(netflow_nodes)))
    ipt_NETFLOW_configurator.configure(netgraph.graph,
                                       collector_node=controller_node,
                                       border_routers=netflow_nodes,
                                       )

    LOG.debug("logging out of all nodes")
    ssh_helper.network_graph_logout(netgraph.graph)
